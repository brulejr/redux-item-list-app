import { ReduxItemListAppPage } from './app.po';

describe('redux-item-list-app App', () => {
  let page: ReduxItemListAppPage;

  beforeEach(() => {
    page = new ReduxItemListAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
