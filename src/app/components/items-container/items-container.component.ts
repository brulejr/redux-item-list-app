/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Store} from '@ngrx/store';

import { AppState, Item } from '../../models';
import { ItemsService } from '../../services/items';
import { SelectItemAction } from '../../stores';

@Component({
  selector: 'items-container',
  templateUrl: './items-container.component.html',
  styleUrls: ['./items-container.component.less']
})
export class ItemsContainerComponent implements OnInit {

  items: Observable<Array<Item>>;
  selectedItem: Observable<Item>;

  constructor(
    private _itemsService: ItemsService,
    private _store: Store<AppState>
  ) {
    this.items = _itemsService.items;
    this.selectedItem = _store.select('selectedItem');
  }

  ngOnInit(): void {
    this._itemsService.loadItems();
  }

  deleteItem(item: Item) {
    this._itemsService.deleteItem(item).then((v) => {
      this.resetItem();
    });
  }

  resetItem() {
    let emptyItem: Item = {id: null, name: '', description: ''};
    this._store.dispatch(new SelectItemAction(emptyItem));
  }

  saveItem(item: Item) {
    this._itemsService.saveItem(item).then((v) => {
      this.resetItem();
    });
  }

  selectItem(item: Item) {
    this._store.dispatch(new SelectItemAction(item));
  }

}
