/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Action } from '@ngrx/store';

import { Item } from '../models';

export const ITEMS_INITIAL_STATE = <Item[]>[];

const CREATE_ITEM = 'items/CREATE_ITEM';
const DELETE_ITEM = 'items/DELETE_ITEM';
const LOAD_ITEMS = 'items/LOAD_ITEMS';
const UPDATE_ITEM = 'items/UPDATE_ITEM';

export class CreateItemAction implements Action {
  readonly type = CREATE_ITEM;
  constructor(public payload: Item) { }
}

export class DeleteItemAction implements Action {
  readonly type = DELETE_ITEM;
  constructor(public payload: Item) { }
}

export class LoadItemsAction implements Action {
  readonly type = LOAD_ITEMS;
  constructor(public payload: Item[]) { }
}

export class UpdateItemAction implements Action {
  readonly type = UPDATE_ITEM;
  constructor(public id: number, public payload: Item) { }
}

export type ItemActions
  = CreateItemAction
  | DeleteItemAction
  | LoadItemsAction
  | UpdateItemAction;

export function itemsReducer(state: Item[] = ITEMS_INITIAL_STATE, action: ItemActions) {
  switch (action.type) {
    case CREATE_ITEM:
      return [...state, Object.assign({}, action.payload)];
    case DELETE_ITEM:
      return state.filter(item => {
        return item.id !== action.payload.id;
      });
    case LOAD_ITEMS:
      return action.payload;
    case UPDATE_ITEM:
      return state.map(item => {
        return item.id === action.id ? Object.assign({}, item, action.payload) : item;
      });
    default:
      return state;
  }
}
