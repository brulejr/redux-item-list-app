import { TestBed, async } from '@angular/core/testing';

import { Item } from '../models';
import {
  ITEM_INITIAL_STATE,
  SelectItemAction,
  selectedItemReducer } from './selected-item.store';

describe('Store: selectedItemReducer', () => {

  const ITEM: Item = <Item>{
    id: 1234, name: 'TEST1', description: 'This is Test 1'
  };

  describe('SelectItemAction', () => {
    it('should return the provided payload', () => {
      const action = new SelectItemAction(ITEM);
      let defaultState = selectedItemReducer(undefined, action);
      expect(defaultState).toEqual(ITEM);
    });
  });

});
