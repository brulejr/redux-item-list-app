import { TestBed, async } from '@angular/core/testing';

import { Item } from '../models';
import {
  CreateItemAction,
  DeleteItemAction,
  ITEMS_INITIAL_STATE,
  LoadItemsAction,
  UpdateItemAction,
  itemsReducer } from './items.store';

describe('Store: itemsReducer', () => {

  const ITEM1: Item = <Item>{
    id: 1234, name: 'TEST1', description: 'This is Test 1'
  };
  const ITEM2: Item = <Item>{
    id: 5678, name: 'TEST2', description: 'This is Test 2'
  };
  const ITEM3: Item = <Item>{
    id: 9123, name: 'TEST3', description: 'This is Test 3'
  };
  const ITEM4: Item = <Item>{
    id: 4567, name: 'TEST4', description: 'This is Test 4'
  };

  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const result = itemsReducer(undefined, action);
      expect(result).toEqual(ITEMS_INITIAL_STATE);
    });
  });

  describe('CreateItemAction', () => {
    it('should create a single item list by default', () => {
      const action = new CreateItemAction(ITEM1);
      const result = itemsReducer(undefined, action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(1);
      expect(result[0] === ITEM1).toBeFalsy();
      expect(result[0].id).toEqual(ITEM1.id);
      expect(result[0].name).toEqual(ITEM1.name);
      expect(result[0].description).toEqual(ITEM1.description);
    });
    it('should append item to end of list', () => {
      const action = new CreateItemAction(ITEM3);
      const result = itemsReducer([ ITEM1, ITEM2 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(3);
      expect(result[0]).toEqual(ITEM1);
      expect(result[1]).toEqual(ITEM2);
      expect(result[2]).toEqual(ITEM3);
    });
  });

  describe('DeleteItemAction', () => {
    it('should do nothing if list is empty', () => {
      const action = new DeleteItemAction(ITEM1);
      const result = itemsReducer(undefined, action);
      expect(result).toEqual(ITEMS_INITIAL_STATE);
    });
    it('should delete from the beginning of the list', () => {
      const action = new DeleteItemAction(ITEM1);
      const result = itemsReducer([ ITEM1, ITEM2, ITEM3 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(2);
      expect(result[0]).toEqual(ITEM2);
      expect(result[1]).toEqual(ITEM3);
    });
    it('should delete from the middle of the list', () => {
      const action = new DeleteItemAction(ITEM2);
      const result = itemsReducer([ ITEM1, ITEM2, ITEM3 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(2);
      expect(result[0]).toEqual(ITEM1);
      expect(result[1]).toEqual(ITEM3);
    });
    it('should delete from the end of the list', () => {
      const action = new DeleteItemAction(ITEM3);
      const result = itemsReducer([ ITEM1, ITEM2, ITEM3 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(2);
      expect(result[0]).toEqual(ITEM1);
      expect(result[1]).toEqual(ITEM2);
    });
  });

  describe('LoadItemsAction', () => {
    it('should return the payload if state is empty', () => {
      const action = new LoadItemsAction([ ITEM1, ITEM2, ITEM3 ]);
      const result = itemsReducer(undefined, action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(3);
      expect(result[0]).toEqual(ITEM1);
      expect(result[1]).toEqual(ITEM2);
      expect(result[2]).toEqual(ITEM3);
    });
    it('should return the payload even if state is populated', () => {
      const action = new LoadItemsAction([ ITEM1, ITEM2, ITEM3 ]);
      const result = itemsReducer([ ITEM4 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(3);
      expect(result[0]).toEqual(ITEM1);
      expect(result[1]).toEqual(ITEM2);
      expect(result[2]).toEqual(ITEM3);
    });
  });

  describe('UpdateItemAction', () => {
    it('should return the default state if state is empty', () => {
      const action = new UpdateItemAction(0, ITEM4);
      const result = itemsReducer(undefined, action);
      expect(result).toEqual(ITEMS_INITIAL_STATE);
    });
    it('should update an item from the beginning of the list', () => {
      const action = new UpdateItemAction(ITEM1.id, ITEM4);
      const result = itemsReducer([ ITEM1, ITEM2, ITEM3 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(3);
      expect(result[0]).toEqual(ITEM4);
      expect(result[1]).toEqual(ITEM2);
      expect(result[2]).toEqual(ITEM3);
    });
    it('should update an item from the middle of the list', () => {
      const action = new UpdateItemAction(ITEM2.id, ITEM4);
      const result = itemsReducer([ ITEM1, ITEM2, ITEM3 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(3);
      expect(result[0]).toEqual(ITEM1);
      expect(result[1]).toEqual(ITEM4);
      expect(result[2]).toEqual(ITEM3);
    });
    it('should update an item from the end of the list', () => {
      const action = new UpdateItemAction(ITEM3.id, ITEM4);
      const result = itemsReducer([ ITEM1, ITEM2, ITEM3 ], action);
      expect(result).toBeDefined();
      expect(result.length).toEqual(3);
      expect(result[0]).toEqual(ITEM1);
      expect(result[1]).toEqual(ITEM2);
      expect(result[2]).toEqual(ITEM4);
    });
  });

});
