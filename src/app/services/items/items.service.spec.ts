/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import {
  HttpModule,
  Http,
  Response,
  ResponseOptions,
  XHRBackend
} from '@angular/http';
import { Store, StoreModule, provideStore } from '@ngrx/store';

import { ItemsService } from './items.service';
import { AppState, Item } from '../../models';
import { LoadItemsAction, itemsReducer } from '../../stores';

const ITEM1: Item = <Item>{
  id: 1234, name: 'TEST1', description: 'This is Test 1'
};
const ITEM2: Item = <Item>{
  id: 5678, name: 'TEST2', description: 'This is Test 2'
};
const ITEM3: Item = <Item>{
  id: 9123, name: 'TEST3', description: 'This is Test 3'
};
const ITEM4: Item = <Item>{
  id: 4567, name: 'TEST4', description: 'This is Test 4'
};

describe('Service: ItemsService', () => {

  let itemsService: ItemsService;
  let mockBackend: MockBackend;
  let store: Store<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        StoreModule.provideStore({ items: itemsReducer })
      ],
      providers: [
        ItemsService,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    });
    itemsService = TestBed.get(ItemsService);
    mockBackend = TestBed.get(XHRBackend);
    store = TestBed.get(Store);
  });

  it('should be defined', () => {
   expect(itemsService).toBeDefined();
  });

  describe('createItem()', () => {
    it('should add an item to the items store', fakeAsync((): void => {

      const action = new LoadItemsAction([ITEM1, ITEM2]);
      store.dispatch(action);

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(ITEM3)
        })));
      });

      itemsService.createItem(ITEM3);
      tick();

      store.select('items').subscribe(items => {
        expect(items).toBeDefined();
        expect(items[0]).toEqual(ITEM1);
        expect(items[1]).toEqual(ITEM2);
        expect(items[2]).toEqual(ITEM3);
        expect(items[3]).toBeUndefined();
      });

    }));
  });

  describe('deleteItem()', () => {
    it('should delete an item from the items store', fakeAsync((): void => {

      const action = new LoadItemsAction([ITEM1, ITEM2, ITEM3]);
      store.dispatch(action);

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({})));
      });

      itemsService.deleteItem(ITEM2);
      tick();

      store.select('items').subscribe(items => {
        console.log('items', items);
        expect(items).toBeDefined();
        expect(items[0]).toEqual(ITEM1);
        expect(items[1]).toEqual(ITEM3);
        expect(items[2]).toBeUndefined();
      });

    }));
  });

  describe('loadItems()', () => {
    it('should replace all items to the items store', fakeAsync((): void => {

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify([ ITEM1, ITEM2, ITEM3, ITEM4 ])
        })));
      });

      itemsService.loadItems();
      tick();

      store.select('items').subscribe(items => {
        expect(items).toBeDefined();
        expect(items[0]).toEqual(ITEM1);
        expect(items[1]).toEqual(ITEM2);
        expect(items[2]).toEqual(ITEM3);
        expect(items[3]).toEqual(ITEM4);
        expect(items[4]).toBeUndefined();
      });

    }));
  });

  describe('updateItem()', () => {
    it('should update an item in the items store', fakeAsync((): void => {

      const action = new LoadItemsAction([ITEM1, ITEM2, ITEM3]);
      store.dispatch(action);

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(ITEM4)
        })));
      });

      itemsService.updateItem(Object.assign({}, ITEM4, {id: ITEM2.id}));
      tick();

      store.select('items').subscribe(items => {
        expect(items).toBeDefined();
        expect(items[0]).toEqual(ITEM1);
        expect(items[1]).toBeDefined();
        expect(items[1].id).toEqual(ITEM2.id);
        expect(items[1].name).toEqual(ITEM4.name);
        expect(items[1].description).toEqual(ITEM4.description);
        expect(items[2]).toEqual(ITEM3);
        expect(items[3]).toBeUndefined();
      });

    }));
  });

 });
