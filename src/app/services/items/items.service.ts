/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { AppState, Item } from '../../models';
import { CreateItemAction, DeleteItemAction, LoadItemsAction, UpdateItemAction } from '../../stores';

const BASE_URL = 'http://localhost:3000/items/';
const HEADERS = { headers: new Headers({ 'Content-Type': 'application/json' }) };

@Injectable()
export class ItemsService {

  items: Observable<Array<Item>>;

  constructor(private _http: Http, private _store: Store<AppState>) {
    this.items = _store.select('items');
  }

  createItem(item: Item): Promise<any> {
    let promise: Promise<any> = new Promise((resolve: any) => {
      this._http.post(BASE_URL, JSON.stringify(item), HEADERS)
        .map(res => res.json())
        .map(payload => new CreateItemAction(payload))
        .subscribe(action => {
          this._store.dispatch(action);
          resolve();
        });
    });
    return promise;
  }

  deleteItem(item: Item): Promise<any> {
    let promise: Promise<any> = new Promise((resolve: any) => {
      this._http.delete(BASE_URL + item.id)
        .subscribe(action => {
          this._store.dispatch(new DeleteItemAction(item));
          resolve();
        });
    });
    return promise;
  }

  loadItems(): Promise<any> {
    let promise: Promise<any> = new Promise((resolve: any) => {
      this._http.get(BASE_URL)
        .map(res => res.json())
        .map(payload => new LoadItemsAction(payload))
        .subscribe(action => {
          this._store.dispatch(action);
          resolve();
        });
    });
    return promise;
  }

  saveItem(item: Item): Promise<any> {
    return (item.id) ? this.updateItem(item) : this.createItem(item);
  }

  updateItem(item: Item): Promise<any> {
    let promise: Promise<any> = new Promise((resolve: any) => {
      this._http.put(BASE_URL + item.id, JSON.stringify(item), HEADERS)
        .subscribe(action => {
          this._store.dispatch(new UpdateItemAction(item.id, item));
          resolve();
        });
    });
    return promise;
  }

}
