/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { EventService } from './event.service';

describe('Service: EventService', () => {

  let service: EventService;

  beforeEach(() => {
    service = new EventService();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('#on should return an Observable', () => {
    let observable = service.on('EVENT');
    expect(observable).toBeTruthy();
  });

  it('#post should send a message to a matching event handler', done => {
    let count = 2;
    let subscription1 = service.on('TEST1').subscribe((event) => {
      expect(event).toBeDefined();
      expect(event.type).toBe('TEST1');
      expect(event.data).toBeDefined();
      expect(event.data.name).toBe('test.event.1');
      count -= 1;
    });
    let subscription2 = service.on('TEST2').subscribe((event) => {
      expect(event).toBeDefined();
      expect(event.type).toBe('TEST2');
      expect(event.data).toBeDefined();
      expect(event.data.name).toBe('test.event.2');
      count -= 1;
    });

    service.post('TEST1', {name: 'test.event.1'});
    service.post('TEST2', {name: 'test.event.2'});

    if (count === 0) {
      subscription1.unsubscribe();
      subscription2.unsubscribe();
      done();
    }
  });

  it('#post should not fail when no handlers are defined', () => {
    service.post('TEST_NO_HANDLERS', {name: 'test.event'});
  });

});
